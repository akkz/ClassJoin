<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="akkz.*" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>比赛信息</title>
	
	<link type="text/css" rel="stylesheet" href="/css/main.css" />
	<link type="text/css" rel="stylesheet" href="/css/manage.css" />
	<link type="text/css" rel="stylesheet" href="/css/userInf.css" />
	<link href="/SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css">
	<script src="/SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
</head>


<body>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>
		
		<% 
			Student student = (Student) request.getSession().getAttribute("student"); 
			
			if(!student.getMajor().contains("admin"))
			{
				response.sendRedirect("/jsp/user.jsp");
				System.out.print(student.getMajor());
				
				return;
			}
		
		
			StudentServer studentServer = (StudentServer)this.getServletContext().getAttribute("studentServer");
		
			ArrayList<Classes> classes = studentServer.getAllClass();
		%>
		<div id="mainBody">
			<div id="leftBody">
			
			<%
				for(Classes classnum : classes)
				{
			%>
				<div id="CollapsiblePanel<%= classnum.getId() %>" class="CollapsiblePanel">
					<div class="CollapsiblePanelTab" tabindex="0"><%= classnum.getName() %></div>
					<div class="CollapsiblePanelContent">
					<table class="stuInf">
					<%
						for(Student stu : studentServer.getStudents(classnum.getId()))
						{
					%>
					<tr>
						<td><%= stu.getId() %></td><td><%= stu.getName() %></td><td><%= stu.getMajor() %></td><td><%= stu.getTel() %></td><td><%= stu.getMail() %></td>
					</tr>
					<%
						}
					%>
					</table>
					</div>
				</div>
			<%
				}
			%>			
			</div>
			
			<div id="userInf">
				<jsp:include page="/jsp/userInf.jsp"></jsp:include>
			</div>
		</div>

		<jsp:include page="/jsp/foot.jsp"></jsp:include>
	</div>
<script type="text/javascript">
<%
	for(Classes classnum : classes)
	{
%>
var CollapsiblePanel<%= classnum.getId()%> = new Spry.Widget.CollapsiblePanel("CollapsiblePanel<%= classnum.getId() %>");
<%
	}
%>
	</script>
</body>


</html>