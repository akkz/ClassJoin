<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="akkz.*" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>比赛信息</title>
	
	<link type="text/css" rel="stylesheet" href="/css/main.css" />
	<link type="text/css" rel="stylesheet" href="/css/user.css" />
	<link type="text/css" rel="stylesheet" href="/css/userInf.css" />
	
	<script type="text/javascript">
		function regist(match)
		{
			var res = confirm("您确定取消报名吗？");

			if(res)
			{
				document.getElementById("classid").value = match;
				
				document.getElementById("regForm").submit();
			}
		}
	</script>
</head>


<body>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>
		
		<% 
			Student student = (Student) request.getSession().getAttribute("student"); 
			StudentServer studentServer = (StudentServer)this.getServletContext().getAttribute("studentServer");
		
			ArrayList<Classes> classes = studentServer.getClasses(student.getId());
		%>
		<div id="mainBody">
			<div id="leftBody">
				<div class="matchTitleFloat" id="matchInfTitle">
					<div class="matchTitleFloat" id="matchInfTitleLeft">
						<div id="matchInfTitleFont">报名信息</div>
					</div>
					
					<div class="matchTitleFloat" id="matchInfTitleRight">
						<div id="matchInfTip">您已经参加</div>

					</div>
				</div>
				
			<form name="regForm" id="regForm" action="/cancle.do" method="post" >
				<input type="hidden" name="classid" id="classid" value="2" >
			</form>
				
					<%
						String matchGo = null;
						String divKind = null;
					
						for ( Classes match : classes )
						{
					%>
				<div class="matchSelect1" onClick="regist(<%=match.getId() %>)">
					<div class="matchTitle"><%= match.getName() %>&nbsp;&nbsp;&nbsp;&nbsp;<div style="font-size:24px;"><%= match.getDate() %></div></div>
					<div class="matchhost">主讲：<%= match.getHost() %></div>
					<div class="matchPeople">已报名/总共:&nbsp;<%= match.getJoined() %>/45</div>
					<img class="matchTip" src="/img/matchTip.png">
				</div>
					<%
						}
						
						if(classes.size() == 0)
						{
					%>
						
				<div id="noMatch">
					<div class="matchTitle">暂无已报名课堂</div>
					<img class="matchTip" src="/img/matchStop.png">
				</div>
					<%
						}
					%>
			</div>

			<div id="userInf">
				<jsp:include page="/jsp/userInf.jsp"></jsp:include>
				<a href="/jsp/match.jsp"><div id="userJoin">点击前往报名</div></a>
			</div>
		</div>

		<jsp:include page="/jsp/foot.jsp"></jsp:include>
	</div>
</body>


</html>