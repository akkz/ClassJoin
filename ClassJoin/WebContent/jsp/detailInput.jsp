<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>信息输入</title>
	
	<link type="text/css" rel="stylesheet" href="/css/main.css" />
	<link type="text/css" rel="stylesheet" href="/css/detailInput.css" />
	<link type="text/css" rel="stylesheet" href="/css/userInf.css" />
</head>

<body>
	<%
		String security = (String)request.getAttribute("security");

		if(security == null || !security.equals("OK"))
		{
			response.sendRedirect("/jsp/infCheck.jsp");
			return;
		}
	%>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>
		
		<div id="mainBody">
			<div id="leftBody">
				<div id="inputLayout">
					<div id="inputTitle">信息完善</div>
					
					<div id="inputLine"></div>
					
					<form method="post" action="/detailInput.do" name="infInput">
						<div class="infInputLevel">Tel：<input type="text" name="tel" value="${student.tel }" ></div>
						<div class="infInputLevel">Mail：<input type="text" name="mail" value="${student.mail }" ></div>
						<div class="infInputLevel" id="infInputLevelSub">
						<%
							String uTip = request.getParameter("tip");
						
							if(uTip == null)
								uTip = "";
						
							if(uTip.equals("telError")) 
							{
								uTip = "Tip:电话格式不正确";
							}
							else if(uTip.equals("mailError"))
							{
								uTip = "Tip:邮箱格式错误";
							}
						%>
							<%= uTip %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a id="cancle" href="/jsp/user.jsp">取消</a>
							<input id="btn" type="button" value="完成" onClick="document.infInput.submit()">
						</div>
					</form>
				</div>
				
				<div id="inputTip">Tip:您的号码和邮箱将会作为比赛信息的通知地址，我们会确保您的信息安全。（请确保输入正确，再次登录后，手机号码将被加密）</div>
			</div>

			<div id="userInf">
				<jsp:include page="/jsp/userInf.jsp"></jsp:include>
			</div>
			
		<jsp:include page="/jsp/foot.jsp"></jsp:include>

		</div>
	</div>
</body>
</html>