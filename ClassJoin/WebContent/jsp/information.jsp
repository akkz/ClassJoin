<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>信息</title>
	
	<link type="text/css" rel="stylesheet" href="/css/main.css" />
	<link type="text/css" rel="stylesheet" href="/css/information.css" />
	<link type="text/css" rel="stylesheet" href="/css/userInf.css" />
</head>

<body>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>
		
		<div id="mainBody">
			<div id="leftBody">
				<div id="infBodyMain">
					<div id="infBodyTitle">${tip }</div>
					<div id="infBodyContent">${error }</div>
					<div id="infBodyHelp">帮助信息：</div>
					<a href="${backPage }">
						<div id="infBodyBtn">返回</div>
					</a>
				</div>
			</div>

			<div id="userInf">
				<jsp:include page="/jsp/userInf.jsp"></jsp:include>
			</div>
		</div>
		
		<jsp:include page="/jsp/foot.jsp"></jsp:include>
	</div>
</body>
</html>