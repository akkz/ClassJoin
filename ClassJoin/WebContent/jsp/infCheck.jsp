<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>数据检查</title>
	
	<link type="text/css" rel="stylesheet" href="/css/main.css" />
	<link type="text/css" rel="stylesheet" href="/css/detailInput.css" />
	<link type="text/css" rel="stylesheet" href="/css/userInf.css" />
</head>

<body>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>
		
		<div id="mainBody">
			<div id="leftBody">
				<div id="inputLayout">
					<div id="inputLeft">
						<div id="inputTitle">数据检查</div>
					</div>
					<div id="inputLine"></div>
					<div id="inputRight">
							<form method="post" action="/oldTelCheck.do" name="infInput">
								<div class="infInputLevel">旧号码：&nbsp;<input type="text" name="tel" value="" ></div>
								
								<div class="infInputLevel"><input id="btn" type="button" value="完成" onClick="document.infInput.submit()"><a id="cancle" href="/jsp/user.jsp">取消</a></div>
							</form>
					</div>
				</div>
				
				<div id="inputTip">Tip:如有问题，欢迎咨询官方QQ：<a target="_blank" href="http://sighttp.qq.com/authd?IDKEY=6d9e3fc1f6a59a93e5e8327b7477eab55bb86338f9a3be5d"><img border="0" src="http://wpa.qq.com/imgd?IDKEY=6d9e3fc1f6a59a93e5e8327b7477eab55bb86338f9a3be5d&pic=52" alt="官方QQ" title="官方QQ">1774153784</a></div>
			</div>

			<div id="userInf">
				<jsp:include page="/jsp/userInf.jsp"></jsp:include>
			</div>
		</div>
		
		<jsp:include page="/jsp/foot.jsp"></jsp:include>
	</div>
</body>
</html>