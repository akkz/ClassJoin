<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登陆</title>

<link type="text/css" rel="stylesheet" href="/css/main.css">
<link type="text/css" rel="stylesheet" href="/css/login.css">
</head>


<body>
	<div id="contain">
		<jsp:include page="/jsp/head.jsp"></jsp:include>

		<div id="infBody">
			<div id="loginBody">
				<div id="loginInf">
					<div id="loginInfMain">登陆</div>
					<div id="loginInfSecond">login</div>
				</div>

				<div id="space">
					<div class="spaceBlack"></div>
					<div id="spaceWrite"></div>
					<div class="spaceBlack"></div>
				</div>

				<div id="loginForm">
					<form method="post" name="login" action="/login.go">
						<div class="level">
							姓名： <input type="text" name="name" value="">
						</div>

						<div class="level">
							学号： <input type="text" name="id" value="">
						</div>
						<div class="level">
						<%
							String uTip = request.getParameter("tip");
						
							if(uTip == null)
								uTip = "";
						
							if(uTip.equals("idError")) 
							{
								uTip = "Tip:学号格式不正确";
							}
							else if(uTip.equals("equalError"))
							{
								uTip = "Tip:学号或姓名错误";
							}
						%>
							<%= uTip %>
							<input type="button" id="sub" value="提交"
								onClick="document.login.submit()">
						</div>
					</form>
				</div>
			</div>
		</div>

		<jsp:include page="/jsp/foot.jsp"></jsp:include>
	</div>
</body>


</html>