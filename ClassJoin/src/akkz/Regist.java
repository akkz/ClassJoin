﻿package akkz;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Regist extends HttpServlet
{
	private static final long serialVersionUID = 1L;


	public Regist()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		Student student = (Student) request.getSession().getAttribute("student");

		
		int classid = Integer.parseInt(request.getParameter("classid"));

		StudentServer studentServer = (StudentServer) this.getServletContext().getAttribute("studentServer");

		if(studentServer.regist(student.getId(), classid))
		{
			request.setAttribute("error", "恭喜您，报名成功！");
			request.setAttribute("tip", "报名成功！");
			request.setAttribute("backPage", "/jsp/match.jsp");
			
			//Logger.getLogger(Login.class.getName()).log(Level.SEVERE, "TelChange id: " + student.getId() + "\tTelChangeError");
			
			request.getRequestDispatcher("/jsp/information.jsp").forward(request, response);
		}
		else
		{
			request.setAttribute("error", "迟了一步，人数已报满~。~");
			request.setAttribute("tip", "出错啦！");
			request.setAttribute("backPage", "/jsp/match.jsp");
			
			//Logger.getLogger(Login.class.getName()).log(Level.SEVERE, "TelChange id: " + student.getId() + "\tTelChangeError");
			
			request.getRequestDispatcher("/jsp/information.jsp").forward(request, response);
		}
	}

}
