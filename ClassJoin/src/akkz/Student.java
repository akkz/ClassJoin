﻿package akkz;

public class Student
{
	private String name;
	private int id;
	private String tel;
	private String mail;

	private String major;
	
	private String telShow;

	public Student( int id, String name)
	{
		this.name = name;
		this.id = id;
		this.tel = null;
		this.mail = null;
		this.telShow = null;
	}
	
	public Student(int id, String name, String tel, String mail, String major)
	{
		this.name = name;
		this.id = id;
		this.tel = tel;
		this.mail = mail;
		this.major = major;
		initTelShow();
	}
	
	private void initTelShow()
	{
		if(tel != null && tel.length() == 11)
			this.telShow = tel.substring(0, 3) + "****" + tel.substring(7);
		else
			this.telShow = "";
	}

	public void setTel(String tel)
	{
		this.tel = tel;
		initTelShow();
	}

	public String getTelShow()
	{
		return telShow;
	}

	public void setMail(String mail)
	{
		this.mail = mail;
	}

	public String getMail()
	{
		return mail;
	}

	public String getTel()
	{
		return tel;
	}

	public String getName()
	{
		return name;
	}

	public int getId()
	{
		return id;
	}

	public String getMajor()
	{
		return major;
	}
}
