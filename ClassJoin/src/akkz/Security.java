﻿package akkz;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

import org.omg.CORBA.Request;

@WebFilter("/Security")
public class Security implements Filter
{

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{		
		Student student = (Student) ((HttpServletRequest) request).getSession().getAttribute("student");
		if (student == null)
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		else
			chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub
		
	}

}
