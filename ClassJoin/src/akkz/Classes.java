﻿package akkz;

public class Classes
{
	private int id;
	private String name;
	private int joined;
	private String place;
	private String date;
	private String host;


	public Classes(int id, String name, int joined, String place, String date, String host)
	{
		super();
		this.id = id;
		this.name = name;
		this.joined = joined;
		this.place = place;
		this.date = date;
		this.host = host;
	}

	public String getPlace()
	{
		return place;
	}

	public void setPlace(String place)
	{
		this.place = place;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public int getJoined()
	{
		return joined;
	}

	public void setJoined(int joined)
	{
		this.joined = joined;
	}

}
