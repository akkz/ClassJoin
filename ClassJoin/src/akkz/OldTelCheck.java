﻿package akkz;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class OldTelCheck
 */
public class OldTelCheck extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OldTelCheck()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		
		Student student = (Student) request.getSession().getAttribute("student");
		
		if ( request.getParameter("tel").equals(student.getTel()))
		{
			request.setAttribute("security", "OK");
			request.getRequestDispatcher("/jsp/detailInput.jsp").forward(request, response);
		}
		else
		{
			request.setAttribute("error", "号码不对哇~.~|||");
			request.setAttribute("tip", "出错啦！");
			request.setAttribute("backPage", "/jsp/infCheck.jsp");
			
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, "TelChange id: " + student.getId() + "\tTelChangeError");
			
			request.getRequestDispatcher("/jsp/information.jsp").forward(request, response);
		}
	}

}
