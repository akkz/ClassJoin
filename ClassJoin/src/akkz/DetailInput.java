﻿package akkz;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InformationInput
 */

public class DetailInput extends HttpServlet
{
	Student student;
	int id;
	String name;
	
	private static final long serialVersionUID = 1L;

	public DetailInput()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		//Student student = (Student)this.getServletContext().getAttribute("student");
		student = (Student)request.getSession().getAttribute("student");
		
		id = student.getId();
		name = student.getName();
		
		StudentServer studentServer = (StudentServer) this.getServletContext().getAttribute("studentServer");
		
		String tel = request.getParameter("tel");
		String mail = request.getParameter("mail");
		
		if ( !longCheck(11, 11, tel) || !numCheck(tel) ) 
		{			
			Logger.getLogger(DetailInput.class.getName()).log(Level.SEVERE, "telError" + " id:" + id + "\tname:" + name + "\terror: " + tel, id);
			
			request.setAttribute("security", "OK");
			request.getRequestDispatcher("/jsp/detailInput.jsp?tip=telError").forward(request, response);
			
			return;
		}
		
		if (!longCheck(1, 45, mail))
		{
			Logger.getLogger(DetailInput.class.getName()).log(Level.SEVERE, "mailError" + " id:" + id + "\tname:" + name + "\terror: " + tel, id);
			
			request.setAttribute("security", "OK");
			request.getRequestDispatcher("/jsp/detailInput.jsp?tip=mailError").forward(request, response);
			
			return;
		}
		
		studentServer.setStudnet(id, "tel", tel);
		studentServer.setStudnet(id, "mail", mail);

		student.setTel(tel);
		student.setMail(mail);
		
		request.getRequestDispatcher("/jsp/match.jsp").forward(request, response);
	}
	
	private boolean numCheck(String num)
	{
		try
		{
			Long.parseLong(num);
		}
		catch (NumberFormatException e)
		{
			return false;
		}
		
		return true;
	}
	
	private boolean longCheck( int min, int max, String num)
	{
		if(num.length() > max || num.length() < min)
			return false;
		else
			return true;
	}
}
