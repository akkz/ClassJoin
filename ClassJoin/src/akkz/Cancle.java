﻿package akkz;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Cancle extends HttpServlet
{
	private static final long serialVersionUID = 1L;


	public Cancle()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		Student student = (Student) request.getSession().getAttribute("student");

		
		int classid = Integer.parseInt(request.getParameter("classid"));

		StudentServer studentServer = (StudentServer) this.getServletContext().getAttribute("studentServer");

		if(studentServer.cancle(student.getId(), classid))
		{
			request.getRequestDispatcher("/jsp/user.jsp").forward(request, response);
		}
		else
		{
			request.setAttribute("error", "删除失败~。~");
			request.setAttribute("tip", "出错啦！");
			request.setAttribute("backPage", "/jsp/user.jsp");
			
			//Logger.getLogger(Login.class.getName()).log(Level.SEVERE, "TelChange id: " + student.getId() + "\tTelChangeError");
			
			request.getRequestDispatcher("/jsp/information.jsp").forward(request, response);
		}
	}

}
